import Vuex from 'vuex'
import Vue from 'vue'
import axios from 'axios'

Vue.use(Vuex)


const store = new Vuex.Store({
  data() {
    return {
      seaside: undefined,
      count: undefined
    }
  },
  mutations: {
    setSeaside(state, seaside) {
      state.seaside = seaside

    },
    initState(state, seasidedata) {
      state.count = seasidedata.count
    },
    setCount(state, count) {
      state.count = count
    }
  },
  actions: {
    increment(context) {
      let seaside = context.state.seaside
      return axios.get(seaside.callbackUrl + '&' + seaside.callbackValueArg + '=' + JSON.stringify({count: (context.state.count + 1)}))
        .then((result) => {
          context.commit('setCount', result.data.count)
          return result.data.count
        })
    }
  }
})

export default store
