import Vue from 'vue'
import App from './App.vue'
import store from './store/store.js'

Vue.config.productionTip = false


function renderCounter(domElement, seasideconfig, seasidedata) {
  store.commit('setSeaside', seasideconfig)
  store.commit('initState', seasidedata)
  //ReactDOM.render(<Demo {...seasidedata}/>, domElement);
  new Vue({
    store,
    render: h => h(App),
  }).$mount(domElement)

  // ReactDOM.render(React.createElement(component,seasidedata,null),domElement);
  // renderWithSeaside(domElement, Counter, seasideconfig, seasidedata)
}

document.renderCounter = renderCounter
