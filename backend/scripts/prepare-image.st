repo := IceRepositoryCreator new
    location: '..' asFileReference asAbsolute;
    subdirectory: 'backend/source';
    createRepository.

repo 
    name: 'SeasideVuejs';
    register.

Smalltalk tools inspector new 
	title: 'project scripts';
	openOn: 'scripts' asFileReference

