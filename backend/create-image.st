#!/bin/sh

echo "remove old dangling files"
rm Pharo.image Pharo.changes
mv seaside-vue.image seaside-vue.image.bak
mv seaside-vue.changes seaside-vue.changes.bak

echo "download image"
curl get.pharo.org/64/70 | bash

echo "remove old vm files"
rm pharo pharo-ui
rm -rf pharo-vm

echo "downlaod vm"
curl get.pharo.org/64/vm70 | bash

echo "rename Pharo.image to seaside-vue.image"
mv Pharo.image seaside-vue.image
mv Pharo.changes seaside-vue.changes

echo "open image and install"
./pharo-ui ./seaside-vue.image st --save scripts/prepare-image.st